FROM openjdk:8-jre-alpine
COPY target/Api-Investimentos-*.jar api-investimentos.jar
CMD ["java", "-jar", "api-investimentos.jar"]
